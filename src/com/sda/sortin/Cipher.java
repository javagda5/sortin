package com.sda.sortin;

import java.util.Arrays;

public class Cipher {
    public static void main(String[] args) {
        decrypt("WAHITILEKODNIBGSEITNSTOIHGN", 1, 2);
    }

    public static void decrypt(String line, int start, int width) {
        char[] arr = line.toCharArray();
        for (int i = start; i < arr.length - 1; i += width) {
            char[] subarr = Arrays.copyOfRange(arr, i, i + width );

            for (int j = 0; j < subarr.length; j++) {
                int indeksWstawiania = j+1;
                indeksWstawiania = indeksWstawiania%width;
                arr[i+indeksWstawiania] = subarr[j];
            }
        }

        String result = new String(arr);
        System.out.println(result);
    }
}
