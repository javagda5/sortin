package com.sda.sortin;

public class QuickSort {
    public static int[] sort(int[] tab) {

        quick(tab, 0, tab.length - 1);
        return tab;
    }

    public static void quick(int[] tab, int from, int to) {

        int pivot = tab[(to + from) / 2];
        int i = from;
        int j = to;

        do {
            while (tab[i] < pivot) {
                i++;
            }
            while (tab[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int tmp = tab[i];
                tab[i] = tab[j];
                tab[j] = tmp;
                i++;
                j--;
            }
        } while (i <= j);

        if (from < j) {
            quick(tab, from, j);
        }
        if (i < to) {
            quick(tab, i, to);
        }
    }
}
