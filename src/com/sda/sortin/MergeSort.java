package com.sda.sortin;

import java.util.Arrays;

public class MergeSort {
    private static int counter = 0;

    public static void main(String[] args) {
        int[] ar = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
//        int[] ar = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        MergeSort.sort(ar);
        System.out.println(Arrays.toString(ar));
        System.out.println(counter);
    }

    // log2(n) * n
    public static void sort(int[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    private static void mergeSort(int[] array, int from, int to) {
        if (from == to) {
            return;
        }

//        System.out.println("From:" + from + " to:" + to);
        // 7 - 0 = 7 / 2 = 3
        // 7 - 4 = 3 / 2 = 1
        int middle = ((to - from) / 2) + from;

        // zaczynam od posortowania lewej strony
        mergeSort(array, from, middle); // lewa strona

        // przechodze do sortowania prawej strony po tym jak lewa jest juz posortowana
        mergeSort(array, middle + 1, to); // prawa strona


//        System.out.println(Arrays.toString(array));

        // po posortowaniu lewej i prawej strony złączam je (sortując)
        merge(array, from, middle, to);
    }

    private static void merge(int[] array, int from, int middle, int to) {
        int[] tmp = new int[array.length];
        for (int i = from; i <= to; i++) {
            tmp[i] = array[i]; // tablica elementów tymczasowych
        }
//        System.out.print("Merge: ");
//        for (int i = from; i <= to; i++) {
//            System.out.print(tmp[i] + ",");
//        }
//        System.out.println();

        int indexL = from; // indeks tablicy lewej (którą będę łączył z prawą)
        int indexR = middle + 1; // indeks tablicy prawej
        int insertIndex = from; // indeks pozycji na którą wstawiam (najmniejszy/największy) element

        // (indexL != middle + 1) - koniec przedziału lewej podtablicy
        // (indexR <= to) - koniec przedziału prawej podtablicy
        while ((indexL != middle + 1) && (indexR <= to)) {
            counter++;
            // pętla trwa dopóki jest coś do porównywania (lewa i prawa tablica sie nie skonczyla)
            if (tmp[indexL] > tmp[indexR]) {
                array[insertIndex++] = tmp[indexL++]; // wstawiamy element z lewej tablicy
            } else if (tmp[indexL] <= tmp[indexR]) {
                array[insertIndex++] = tmp[indexR++]; // wstawiamy element z prawej tablicy
            }
            // wstawiam mniejszy/większy z dwóch elementów
            // a następnie inkrementuje licznik lewej/prawej strony oraz
            // pozycji do której wstawiam element
        }

        // po tym jak jedna z tablic sie skonczyla, przepisuje pozostałą tablice
        while (indexL != middle + 1) { // lewy nie dotarł do konca
            counter++;
            array[insertIndex++] = tmp[indexL++];
        }
        while ((indexR <= to)) { // prawy nie dotarł do konca
            counter++;
            array[insertIndex++] = tmp[indexR++];
        }
    }
}
