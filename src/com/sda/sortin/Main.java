package com.sda.sortin;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random r = new Random();

        int[] tab = new int[10];
        for (int i = 0; i < 10; i++) {
            tab[i] = r.nextInt(15);
//            tab[i] = 10-i;
        }


        System.out.println(Arrays.toString(QuickSort.sort(tab)));
//        BubbleSort.sort(tab);
//        InsertionSort.sort(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
//        InsertionSort.sort(new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1});
//        BubbleSort.printArray(tab);
//        CounterSort.sort(tab);
//        BubbleSort.printArray(tab);
    }
}
